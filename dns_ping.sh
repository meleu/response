#!/bin/bash

DNS_LOOKUP_HOST=meraki.com
DNS_SERVER_LIST=./server_list
OUTPUT=./output
RESOLVE_CONF=./resolv.conf
TIMEOUT=1s
FREQUENCY=10s

# for SIGINT, let's clean up outselves then exit
trap cleanup EXIT
function cleanup()
{
	echo INFO: Cleanup >&2
	rm -f $DNS_SERVER_LIST
}

function get_dns_server()
{
	# if server list is empty, replenish it again
	[ ! -s $DNS_SERVER_LIST ] && create_dns_server_list

	# let's get the dns server from the list and then remove it from the list
	dns_server=$(head -1 $DNS_SERVER_LIST)
	sed -i -e "/$dns_server/d" $DNS_SERVER_LIST
}

function create_dns_server_list()
{
	echo INFO: Refresh DNS server list >&2
	# get list of dns servers from resolve.conf
	list=$(grep nameserver $RESOLVE_CONF|sed -e 's/nameserver//'|xargs)
	for i in $list; do
		echo $i >> $DNS_SERVER_LIST
	done
}

function time_elapsed()
{
	unset tt
	ts=$(date +%s%N)
	ip=$(timeout $TIMEOUT $@)

	# if timeout, just return without setting tt
	[ $? -ne 0 ] && echo "ERROR: dns lookup timeout (>$TIMEOUT)" >&2 && return

	tt=$((($(date +%s%N) - $ts)/1000000))
}

# main()
while true; do
	get_dns_server
	status="succeeded"
	timestamp=$(date +"%s")
	time_elapsed dig +short $DNS_LOOKUP_HOST @$dns_server

	# no IP == invalid response
	[ -z "$ip" ] && status="failed"

	echo $timestamp,$dns_server,$status,$tt >&1
	sleep $FREQUENCY
done

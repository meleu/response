# Assignment response - 12/12/2020

## Question 1: Top Swap

### Synopsis
This is a ruby script designed to work with certain expected behaviors.  The script is broken and the objective is to troubleshoot and correct the problems.

### A list of changes
- Exclude `/proc/self`   
`lines` variable is updated to exclude /proc/self as that does not include expected pid info
```
lines = `grep '^Swap' /proc/*/smaps |grep -v self 2>/dev/null`.split("\n")
```
- Incorrect regex syntax.  
```
line.match(/\/proc\/(\d+)\/smaps:Swap:\s+(\d+) kB/)
```

- Exclude zero values  
This is part of the assignment constraint, and the function is just missing.  This part can be optimized with `if kb != 0; ... end`
```
    if kb == 0
	    next
    end
```

- Update output format  
Use `printf` formating option to align output vertically
```
printf "%20.20s  %6.6s %s\n", "Swap space", "PID", "Process"
printf "%20.20s  %6.6s %s\n", "==========", "=====", "======="
```
```
    printf "%17.17s kB  %6.6s %s\n", kb, pid, psout
```

Output (using memory as example)
```
          Swap space     PID Process
          ==========   ===== =======
           262888 kB     314 /usr/sbin/rsyslogd -n
           162696 kB   16275 foreman: master
           156752 kB   16281 unicorn master -c unicorn.conf
            93644 kB   16286 unicorn worker[0] -c unicorn.conf
            91556 kB     322 nginx: worker process
            91556 kB     323 nginx: worker process
            91556 kB     324 nginx: worker process
            91556 kB     325 nginx: worker process
            91192 kB     321 nginx: master process /usr/sbin/nginx -g daemon on; master_process on;
            85392 kB   16284 nginx: worker process
```

### Notes
- This is my first time working with/analyzing Ruby code.  I actually spent a few hours in Udemy prior to taking the assignment.  Ruby is surprisingly easy to learn and a powerful scripting language.
- The assignment is suppose to monitor swap usage.  Due to lack of swap activity, I switch to monitor memory (`Size` instead of `Swap`), or else the output will be empty.  I left the code looking at memory on purpose.  To switch back to swap, just comment/uncomment the lines accordingly.

---
## Question 2: DNS monitoring

### Synopsis
An exercise to write a simple script to perform DNS query and output result according to certain constraints

### List of files
> - [dns_ping.sh](dns_ping.sh)
> - [resolv.conf](resolv.conf)

### How to run
Outputs everything:
```
dns_ping.sh
```
Only cvs output (stdout)
```
dns_ping.sh 2>/dev/null
```
info and error (stderr)
```
dns_ping.sh >/dev/null
```

### Notes
All options are available at the beginning of the script  
- `DNS_LOOKUP_HOST`: Specify a target hostname (I just noticed I have a typo in the actual value... :sweat_smile:).  
- `DNS_SERVER_LIST`: The file stores a list of DNS servers to be used by the script. The list will get smaller as the script retrieves and deletes the DNS server from the file. Once the file is empty, the script will replenish the list from `RESOLVE_CONF`.  This is to guarantee that all DNS servers in `RESOLVE_CONF` are used.  The file is part of the cleanup on script exit.
- ~~`OUTPUT`: not in use~~  
- `RESOLVE_CONF`: The file contains a list of nameserver entries.  In production, this should just simply point to the actual `/etc/resolv.conf`.  For the purpose of the exercise, I just copy nameserver from /etc/resolv.conf and add a list of public nameservers, including a dummy one.  
- `TIMEOUT`: DNS query timeout duration (1s)  
- `FREQUENCY`: Time lapse between each DNS query (10s)  

I actaully cheated on this exercise.  Since the question is not constrained to a given environment, and I have a preview of the question, I completed this exercise the night before.  It took me about an hour to write the script, and another hour to cleanup and format (stress-free and OCD).  In hindsight, I should have studied Ruby first and use Ruby to complete this question and that will make this exercise more fun.

---
## Question 3: The unicorn isn't real

### Synopsis
Troubleshoot 400 error when request includes a client certificate

### Notes
- Up until the end of the session, I am only able to conclude the following:
  - 400 only happens when setting `x-client-ssl-cert` header
    ```
    proxy_set_header X-Client-SSL-Cert $ssl_client_cert
    ```
  - I enabled additional SSL params logging, and examine what values are getting passed through
  - I am not getting cert error from `openssl s_client -connect localhost:8081 -cert ./client.pem </dev/null`. I do notice `SSL_connect:unknown state` when turning on debug
  - Initial SSL connection handshake is using the deprecated SSLv2/v3 (`SSL_connect:SSLv2/v3 write client hello A`); however, it is later switched to TLSv1.2
- Things I will do to continue troubleshooting:
  - This is something I have tried during the assignment; however, I notice that I was using an incorrect port number.  Basically, I can simulate how nginx interacts with the backend service by passing the same header values using curl and see if I get the same 400 error.  This will quickly tell me where the error is coming from.
  - SSL connection is using the deprecated SSLv2/v3 at the beginning. I'd try forcing the client connection using `tls1_2`
  - `client.pem` includes both cert and key (something I noticed afterwards).  I'd breakup the file into individual files and retry with `curl -i -k -E client.crt --key client.key https://localhost:8080`
  - Enable additional logging to check `$ssl_client_verify` status (success or fail)
- Something interesting happened.  I believe a penatration test took place during the exercise, my nginx instance was responding to A LOT of bad/failed requests from `64.39.99.160`.
